import {
  VStack,
  Heading,
  Text,
  Button,
  HStack,
  Box,
  Spacer,
  IconButton,
  useBreakpointValue,
  Tooltip,
} from "@chakra-ui/react";
import { useQuery } from "react-query";
import axios from "axios";
import Link from "next/link";
import { AiOutlineEdit } from "react-icons/ai";

import Card from "../components/Card";

export default function Posts() {
  const { data, isLoading, isError } = useQuery("posts", () =>
    axios
      .get(`${process.env.NEXT_PUBLIC_API_URL}/posts`)
      .then((res) => res.data)
  );

  const isLargeScreen = useBreakpointValue({ base: false, lg: true });

  if (isLoading) {
    return <Text>Loading...</Text>;
  }

  if (isError) {
    return <Text>Failed to load posts</Text>;
  }

  return (
    <VStack
      spacing="4"
      padding="10"
      align="stretch"
      maxW="4xl"
      mx="auto"
      px="4"
    >
      <HStack w="100%" justify="space-between" align="center">
        <Heading as="h1" size="xl">
          Posts
        </Heading>
        <Box>
          <Link href="/posts/create">
            <Button size="lg" mr={4}>
              Create Post
            </Button>
          </Link>
        </Box>
      </HStack>
      {data.map((post) => (
        <Card key={post.id}>
          <VStack align="start" spacing="2">
            <HStack w="100%" justify="space-between">
              <Heading as="h2" size="lg">
                {post.title}
              </Heading>
              <HStack>
                {isLargeScreen && (
                  <Link href={`/posts/${post.id}`} passHref>
                    <Tooltip label="Edit" aria-label="Edit">
                      <IconButton
                        as="a"
                        variant="ghost"
                        size="md"
                        icon={<AiOutlineEdit />}
                        colorScheme="teal"
                      />
                    </Tooltip>
                  </Link>
                )}
              </HStack>
            </HStack>
            <Text fontSize="md">{post.body}</Text>
          </VStack>
        </Card>
      ))}
    </VStack>
  );
}
