import {
  VStack,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Button,
  Text,
  Center,
} from "@chakra-ui/react";
import { useForm } from "react-hook-form";
import { useMutation } from "react-query";
import axios from "axios";
import { useRouter } from "next/router";

export default function CreatePost() {
  const router = useRouter();
  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm();
  const createPostMutation = useMutation((data) =>
    axios
      .post(`${process.env.NEXT_PUBLIC_API_URL}/posts`, data)
      .then((res) => res.data)
  );

  const onSubmit = (data) => {
    createPostMutation.mutate(data);
    router.push("/posts");
  };

  const onCancel = () => {
    router.push("/posts");
  };

  return (
    <Center h="100vh">
      <VStack
        spacing="4"
        padding="8"
        alignItems="stretch"
        bg="white"
        boxShadow="md"
        borderRadius="md"
        w="xl"
      >
        <Heading size="lg">Create Post</Heading>
        <form onSubmit={handleSubmit(onSubmit)} style={{ width: "100%" }}>
          <FormControl isInvalid={errors.title} isRequired>
            <FormLabel>Title</FormLabel>
            <Input {...register("title")} size="md" variant="outline" />
            {errors.title && <Text color="red">Title is required</Text>}
          </FormControl>
          <FormControl isInvalid={errors.body} isRequired mt="4">
            <FormLabel>Body</FormLabel>
            <Input {...register("body")} size="md" variant="outline" />
            {errors.body && <Text color="red">Body is required</Text>}
          </FormControl>
          <Button
            type="submit"
            colorScheme="blue"
            isLoading={createPostMutation.isLoading}
            mt="4"
            w="100%"
          >
            Create
          </Button>
          <Button
            type="button"
            colorScheme="gray"
            variant="outline"
            onClick={onCancel}
            mt="4"
            w="100%"
          >
            Cancel
          </Button>
        </form>
      </VStack>
    </Center>
  );
}
