import { useRouter } from "next/router";
import { useForm } from "react-hook-form";
import { useQuery, useMutation } from "react-query";
import {
  Box,
  Heading,
  FormControl,
  FormLabel,
  Input,
  Textarea,
  Button,
  Center,
} from "@chakra-ui/react";
import axios from "axios";

const EditPost = () => {
  const router = useRouter();
  const { id } = router.query;

  const { data: postData } = useQuery(["post", id], async () => {
    const { data } = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL}/posts/${id}`
    );
    return data;
  });

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    defaultValues: {
      title: postData?.title,
      body: postData?.body,
    },
  });

  const { mutate } = useMutation((post) =>
    axios.put(`${process.env.NEXT_PUBLIC_API_URL}/posts/${id}/edit`, post)
  );

  const onSubmit = (data) => {
    mutate(data, {
      onSuccess: () => {
        router.push("/posts");
      },
    });
  };

  return (
    <Center>
      <Box w="md" mt={10}>
        <Heading>Edit Post</Heading>
        <form onSubmit={handleSubmit(onSubmit)}>
          <FormControl mt={4}>
            <FormLabel>Title</FormLabel>
            <Input
              {...register("title", { required: true })}
              defaultValue={postData?.title}
            />
            {errors.title && <Box color="red">Title is required</Box>}
          </FormControl>
          <FormControl mt={4}>
            <FormLabel>Body</FormLabel>
            <Textarea
              {...register("body", { required: true })}
              defaultValue={postData?.body}
            />
            {errors.body && <Box color="red">Body is required</Box>}
          </FormControl>
          <Button mt={4} colorScheme="teal" type="submit">
            Submit
          </Button>
        </form>
      </Box>
    </Center>
  );
};

export default EditPost;
