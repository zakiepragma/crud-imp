import axios from "axios";

export default async (req, res) => {
  try {
    const response = await axios.get(
      `${process.env.NEXT_PUBLIC_API_URL}/posts`
    );
    const posts = response.data;
    res.status(200).json(posts);
  } catch (error) {
    console.error(error);
    res.status(error.response.status || 500).json({
      error: "Server Error",
    });
  }
};
