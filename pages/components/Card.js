import { Box } from "@chakra-ui/react";

export default function Card({ children }) {
  return (
    <Box
      bg="white"
      boxShadow="md"
      borderRadius="md"
      p="6"
      _hover={{ boxShadow: "lg" }}
      transition="box-shadow 0.2s ease-out"
    >
      {children}
    </Box>
  );
}
